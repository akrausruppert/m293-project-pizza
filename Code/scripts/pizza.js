import jsonData from "../data/pizzas.json" assert { type: "json" };

const container = document.getElementById("container");
const cart = document.getElementById("cart");
let count = 0;

/* Style for the products */
jsonData.forEach((pizza) => {
  const content = document.createElement("div");
  content.classList.add("content");

  const img = document.createElement("img");
  img.src = pizza.imageUrl;

  const row = document.createElement("div");
  row.classList.add("row");

  const item1 = document.createElement("p");
  item1.classList.add("item1");
  item1.textContent = pizza.name;

  const item2 = document.createElement("p");
  item2.classList.add("item2");
  item2.textContent = pizza.prize;

  const button = document.createElement("button");
  button.classList.add("btn");

  const shoppingCartImg = document.createElement("img");
  shoppingCartImg.src = "media/shoppingcart.png";
  shoppingCartImg.classList.add("shoppingcart");

  button.appendChild(shoppingCartImg);

  const item3 = document.createElement("p");
  item3.classList.add("item3");
  item3.textContent = pizza.ingredients.join(", ");
  item3.style.whiteSpace = "normal";

  /* Functions of the shoppingcarts */
  button.addEventListener("click", function () {
    count++;
    cart.textContent = count + " item(s)";

    const resetShoppingCartImg = document.createElement("img");
    resetShoppingCartImg.src = "media/shoppingcart.png";
    resetShoppingCartImg.classList.add("shoppingcart");

    resetShoppingCartImg.addEventListener("click", function () {
      count = 0;
      cart.textContent = "";
      alert("Thank you for your order!");
    });

    cart.appendChild(resetShoppingCartImg);
  });

  row.appendChild(item1);
  row.appendChild(item2);
  row.appendChild(button);

  content.appendChild(img);
  content.appendChild(row);
  content.appendChild(item3);

  container.appendChild(content);
});
