import jsonData from "../data/salads.json" assert { type: "json" };

const container = document.getElementById("container");
const cart = document.getElementById("cart");
let count = 0;

/* Style for the products */
jsonData.forEach((salad) => {
  const content = document.createElement("div");
  content.classList.add("content");

  const img = document.createElement("img");
  img.src = salad.imageUrl;

  const row = document.createElement("div");
  row.classList.add("row");

  const item1 = document.createElement("p");
  item1.classList.add("item1");
  item1.textContent = salad.name;

  const item2 = document.createElement("p");
  item2.classList.add("item2");
  item2.textContent = salad.prize;

  const button = document.createElement("button");
  button.classList.add("btn");

  const shoppingCartImg = document.createElement("img");
  shoppingCartImg.src = "media/shoppingcart.png";
  shoppingCartImg.classList.add("shoppingcart");

  button.appendChild(shoppingCartImg);

  const item3 = document.createElement("p");
  item3.classList.add("item3");
  item3.textContent = salad.ingredients.join(", ");

  /* Functions of the shoppingcarts */
  button.addEventListener("click", function () {
    count++;
    cart.textContent = count + " item(s)";

    const resetShoppingCartImg = document.createElement("img");
    resetShoppingCartImg.src = "media/shoppingcart.png";
    resetShoppingCartImg.classList.add("shoppingcart");

    resetShoppingCartImg.addEventListener("click", function () {
      count = 0;
      cart.textContent = "";
      alert("Thank you for your order!");
    });

    cart.appendChild(resetShoppingCartImg);
  });

  /* Dropdown menu */
  const dressingDropdown = document.createElement("select");
  salad.dressings.forEach((dressing) => {
  const option = document.createElement("option");
    option.value = dressing;
    option.textContent = dressing;
    dressingDropdown.appendChild(option);
    });
  
  dressingDropdown.addEventListener("change", function () {
    const selectedDressing = dressingDropdown.value;
    console.log(`Selected dressing for ${salad.name}: ${selectedDressing}`);
  });

  row.appendChild(item1);
  row.appendChild(item2);
  row.appendChild(button);

  content.appendChild(img);
  content.appendChild(row);
  content.appendChild(item3);
  content.appendChild(dressingDropdown);

  container.appendChild(content);
});
